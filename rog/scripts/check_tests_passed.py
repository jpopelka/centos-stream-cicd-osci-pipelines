#!/usr/bin/python3

import argparse
import sys
import json


def main():
    parser = argparse.ArgumentParser(
        description="Check whether all tests passed (status code 0) or not (status code 1)."
    )
    parser.add_argument("--status-json", required=True, help="Pipeline status JSON")

    args = parser.parse_args()

    status_json = {}
    with open(args.status_json) as f:
        status_json =json.load(f)

    if not status_json:
        print(f"{args.status_json} is empty")
        sys.exit(1)

    jobs = status_json.get('jobs', {})
    tests_pipeline = status_json.get('pipelines', {}).get('trigger_tests', {})

    # All jobs running in the "test" stage of the downstream pipeline are considered to be test jobs
    test_jobs = {
        x: jobs[x] for x in jobs
        if jobs[x].get('stage') == 'test'
        and jobs[x].get('pipeline_id') == tests_pipeline.get('id')
    }
    failed_test_jobs = [x for x in test_jobs if test_jobs[x].get('status') != 'success']

    if failed_test_jobs:
        sys.exit(1)


if __name__ == "__main__":
    main()
