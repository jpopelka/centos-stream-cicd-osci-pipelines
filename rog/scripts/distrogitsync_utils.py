#!/usr/bin/python3

import requests


DISTROGITSYNC_URL = "https://distrogitsync.osci.redhat.com"


def sync_mr_downstream(visibility, project_name, merge_request_iid):
    response = requests.post(
        f"{DISTROGITSYNC_URL}/{visibility}_rpms/rpms/{project_name}/mr/{merge_request_iid}",
        timeout=300,
    )
    response.raise_for_status()


def sync_branch_downstream(namespace, project_name, merge_request_target_branch):
    response = requests.post(
        f"{DISTROGITSYNC_URL}/{namespace}/{project_name}/branches/{merge_request_target_branch}",
        timeout=300,
    )
    response.raise_for_status()
