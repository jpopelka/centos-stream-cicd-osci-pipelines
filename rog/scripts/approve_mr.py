#!/usr/bin/python3

import argparse
import sys

from gitlab_utils import approve_mr, fetch_pipeline_jobs


def main():
    parser = argparse.ArgumentParser(
        description="Approve and comment on a GitLab MR based on pipeline results."
    )
    parser.add_argument("--project-id", required=True, help="GitLab Project ID")
    parser.add_argument("--pipeline-id", required=True, help="GitLab Pipeline ID")
    parser.add_argument("--merge-request-iid", required=True, help="Merge Request IID")
    parser.add_argument("--token", required=True, help="GitLab Private Token")

    args = parser.parse_args()

    headers = {"PRIVATE-TOKEN": args.token, "Content-Type": "application/json"}

    try:
        pipeline_jobs = fetch_pipeline_jobs(
            headers, args.project_id, args.pipeline_id
        )
        check_tickets_status = next(
            (job["status"] for job in pipeline_jobs if job["name"] == "check_tickets"),
            None,
        )
        build_rpm_status = next(
            (job["status"] for job in pipeline_jobs if job["name"] == "build_rpm"),
            None,
        )
        build_centos_stream_rpm_status = next(
            (job["status"] for job in pipeline_jobs if job["name"] == "build_centos_stream_rpm"),
            None,
        )
        if check_tickets_status == "success":
            # y-stream, approve if both builds succeed
            if build_rpm_status == "success" and build_centos_stream_rpm_status == "success":
                print("build_rpm and build_centos_stream_rpm succeeded, approving")
                approve_mr(headers, args.project_id, args.merge_request_iid)
            # z-stream, approve if RHEL build succeeds and there is no CS build
            elif build_rpm_status == "success" and build_centos_stream_rpm_status is None:
                print("build_rpm succeeded and there is no build_centos_stream_rpm job, approving")
                approve_mr(headers, args.project_id, args.merge_request_iid)
            else:
                print("There was an unsuccessful build, not approving")
        else:
            print("check_tickets failed, not approving.")

    except Exception as ex:
        print(f"Error: {ex}")
        sys.exit(1)


if __name__ == "__main__":
    main()
