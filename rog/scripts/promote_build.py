#!/usr/bin/python3

import os
import koji


RHEL_URL = "https://brewhub.engineering.redhat.com/brewhub"
CS_URL = "https://kojihub.stream.rdu2.redhat.com/kojihub"

RHEL_OR_STREAM = os.environ.get("RHEL_OR_STREAM")
CI_COMMIT_SHA = os.environ.get("CI_COMMIT_SHA")
CI_PROJECT_NAME = os.environ.get("CI_PROJECT_NAME")


def main(hub_url):
    brew_session = koji.ClientSession(hub_url)

    builds = brew_session.listBuilds(
        packageID=CI_PROJECT_NAME,
        state=koji.BUILD_STATES["COMPLETE"],
        source=f"*#{CI_COMMIT_SHA}",
        draft=True,
    )
    build = sorted(builds, key=lambda d: d["completion_ts"], reverse=True)[0]
    print(f"Going to promote build {build['nvr']} completed at {build['completion_time']}.")
    brew_session.promoteBuild(build["build_id"])


if __name__ == "__main__":
    main(RHEL_URL)
    # Also promote the Koji (CS) build if we're in CS
    if RHEL_OR_STREAM == 'centos-stream':
        main(CS_URL)
