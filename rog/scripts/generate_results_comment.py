#!/usr/bin/python3

import argparse
import os
import sys
import json

from jinja2 import Environment, FileSystemLoader

COMMENT_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), "data")


def main():
    parser = argparse.ArgumentParser(
        description="Generate the results comment from the pipeline status JSON."
    )
    parser.add_argument("--status-json", required=True, help="Pipeline status JSON")
    parser.add_argument("--output-file", required=False, default="results_comment", help="output file")

    args = parser.parse_args()

    status_json = {}
    with open(args.status_json) as f:
        status_json =json.load(f)

    if not status_json:
        print(f"{args.status_json} is empty")
        sys.exit(1)

    jobs = status_json.get('jobs', {})
    tests_pipeline = status_json.get('pipelines', {}).get('trigger_tests', {})
    check_tickets_job = jobs.get('check_tickets', {})

    build_jobs = {x: jobs[x] for x in jobs if x.startswith('build_')}

    # All jobs running in the "test" stage of the downstream pipeline are considered to be test jobs
    test_jobs = {
        x: jobs[x] for x in jobs
        if jobs[x].get('stage') == 'test'
        and jobs[x].get('pipeline_id') == tests_pipeline.get('id')
    }
    failed_test_jobs = [x for x in test_jobs if test_jobs[x].get('status') != 'success']

    # TODO: The list of required jobs shouldn't be hardcoded here... it should be configurable
    required_jobs = ['check_tickets'] + list(build_jobs.keys())

    is_approved = False
    for required_job in required_jobs:
        if jobs.get(required_job).get('status') != 'success':
            break
    else:
        is_approved = True

    environment = Environment(loader=FileSystemLoader(COMMENT_TEMPLATE_DIR), autoescape=False)

    template = environment.get_template("results_comment.j2")
    results_comment = template.render(
        is_approved=is_approved,
        required_jobs=required_jobs,
        check_tickets_job=check_tickets_job,
        build_jobs=build_jobs,
        test_jobs=test_jobs,
        failed_test_jobs=failed_test_jobs
    )

    with open(args.output_file, "w") as f:
        f.write(results_comment)


if __name__ == "__main__":
    main()
