#!/usr/bin/python3

import os
import sys
import gitlab
import argparse
import json

from gitlab.v4.objects.pipelines import ProjectPipeline
from gitlab.v4.objects import Project


def generate_pipeline_status_json(project: Project, pipeline: ProjectPipeline) -> dict:
    """Generate pipeline status JSON."""
    jobs = pipeline.jobs.list(all=True)

    pipeline_status_json = {
        "pipelines": {
            "main": {
                "id": pipeline.id,
                "status": pipeline.status,
                "url": pipeline.web_url
            }
        },
        "bridges": {},
        "jobs": {}
    }

    for job in jobs:
        job_json = {
            "id": job.id,
            "stage":  job.stage,
            "status": job.status,
            "url": job.web_url,
            "pipeline_id": pipeline.id
        }
        pipeline_status_json["jobs"][job.name] = job_json

    bridges = pipeline.bridges.list(all=True)

    for bridge in bridges:
        bridge_json = {
            "id": bridge.id,
            "stage": bridge.stage,
            "status": bridge.status,
            "url": bridge.web_url,
            "pipeline_id": bridge.pipeline["id"],
            "downstream_pipeline_id": bridge.downstream_pipeline["id"]

        }
        pipeline_status_json["bridges"][bridge.name] = bridge_json

        downstream_pipeline = project.pipelines.get(bridge.downstream_pipeline["id"])
        downstream_pipeline_json = {
            "id": downstream_pipeline.id,
            "status": downstream_pipeline.status,
            "url": downstream_pipeline.web_url
        }

        # pipelines don"t have human-readable names, so we will use the bridge name instead
        pipeline_status_json["pipelines"][bridge.name] = downstream_pipeline_json

        bridge_jobs = downstream_pipeline.jobs.list(all=True)
        for job in bridge_jobs:
            job_json = {
                "id": job.id,
                "stage":  job.stage,
                "status": job.status,
                "url": job.web_url,
                "pipeline_id": bridge.downstream_pipeline["id"]
            }
            pipeline_status_json["jobs"][job.name] = job_json

    return pipeline_status_json


def main():
    parser = argparse.ArgumentParser(
        description="Generate a JSON file containing status of pipelines, bridges and individual jobs."
    )
    parser.add_argument("--project-id", required=True, help="GitLab project ID")
    parser.add_argument("--pipeline-id", required=True, help="GitLab pipeline ID")
    parser.add_argument("--output-file", required=False, default="pipeline_status.json", help="output file")

    args = parser.parse_args()

    if not os.getenv("GITLAB_TOKEN"):
        print("GITLAB_TOKEN is not set")
        sys.exit(1)

    gl = gitlab.Gitlab(private_token=os.getenv("GITLAB_TOKEN"))
    try:
        project = gl.projects.get(args.project_id)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch project {args.project_id} - {e}")
        sys.exit(1)
    try:
        pipeline = project.pipelines.get(args.pipeline_id)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch pipeline {args.pipeline_id} - {e}")
        sys.exit(1)

    pipeline_status_json = generate_pipeline_status_json(project, pipeline)

    with open(args.output_file, "w") as f:
        json.dump(pipeline_status_json, f, indent=4)


if __name__ == "__main__":
    main()
