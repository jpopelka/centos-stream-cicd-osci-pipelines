#!/usr/bin/env python3
"""Use a yaml template to generate a test stage in a CI pipeline for given component."""

import argparse
import os
import sys

import yaml
from jinja2 import Environment, FileSystemLoader

CI_MEDIATOR_CONFIG_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../configs/ci-mediator/config.yaml')
)

JOB_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'data')


class IndentedListDumper(yaml.Dumper):
    """Indent list items in YAML dumps."""

    def increase_indent(self, flow=False, indentless=False):
        """Override the increase_indent function."""
        return super(IndentedListDumper, self).increase_indent(flow, False)


class Literal(str):
    """Represent YAML elements to be dumped as multiline blocks."""

    pass


def literal_presenter(dumper, data):
    """Set dumper style for multiline YAML elements."""
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')


yaml.add_representer(Literal, literal_presenter)


def get_ci_job_yaml_from_template(component: dict, test_configs: dict) -> None:
    """Load a j2 template, render it and save a test_runs.yml file."""
    environment = Environment(loader=FileSystemLoader(JOB_TEMPLATE_DIR), autoescape=True)
    template = environment.get_template('test_stage_template.j2')

    # getting information about tier0 tests presence
    tier0_present = os.environ.get('ROG_TIER0_PRESENT', 'False') == 'True' 

    with open('test_runs.yml', 'w', encoding='utf-8') as outfile_fd:
        for test in component['tests']:
            # Skip tests that are not enabled or are tier0 and tier0 tests are not present
            if test == 'osci.tier0' and not tier0_present:
                print(f'Skipping tier0 test generation as tier0 tests are not present')
                continue
            depends_on = test_configs[test]['depends_on']
            if test_configs[test]['enabled']:
                webhook_url, webhook_ref = '', ''
                try:
                    webhook_url = test_configs[test]['trigger']['webhook']['url']
                    webhook_ref = test_configs[test]['trigger']['webhook']['ref']
                except KeyError:
                    pass
                content = template.render(
                    strategy=test_configs[test]['trigger'].get('strategy', ''),
                    test_name=test,
                    webhook_url=webhook_url,
                    webhook_ref=webhook_ref,
                    depends_on=depends_on,
                )
                test_yaml = yaml.safe_load(content)
                # Fix formatting of multiline sections in final YAML
                for section in ['before_script', 'script', 'after_script']:
                    try:
                        for number, line in enumerate(test_yaml[f'{test}'][section]):
                            if '\n' in line:
                                test_yaml[f'{test}'][section][number] = Literal(line)
                    except KeyError:
                        continue
                yaml.dump(test_yaml, outfile_fd, Dumper=IndentedListDumper, sort_keys=False)
                outfile_fd.write('\n')


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__),
        description='''
            Generate a test stage in a CI pipeline for given component
        ''',
    )
    arg_parser.add_argument(
        'component', help='Component to generate the test stage for.', metavar='COMPONENT'
    )
    args = arg_parser.parse_args()
    if args.component:
        with open(CI_MEDIATOR_CONFIG_PATH, 'r') as f:
            config_yaml = yaml.safe_load(f)
        try:
            component_configs = config_yaml['components']
            test_configs = config_yaml['tests']
        except KeyError as exc:
            print(f'Missing section in configuration file {CI_MEDIATOR_CONFIG_PATH}:', exc)
            sys.exit(1)
        try:
            component = component_configs[args.component]
        except KeyError:
            print(f'No such component: {args.component}')
            sys.exit(1)
        get_ci_job_yaml_from_template(component, test_configs)
