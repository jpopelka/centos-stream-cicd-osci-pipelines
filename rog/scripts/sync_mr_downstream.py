#!/usr/bin/python3

import os

from distrogitsync_utils import sync_mr_downstream


if __name__ == "__main__":

    rhel_or_stream = os.environ.get("RHEL_OR_STREAM")
    project_name = os.environ.get("CI_PROJECT_NAME")
    merge_request_iid = os.environ.get("CI_MERGE_REQUEST_IID")

    if rhel_or_stream == 'rhel':
        visibility = "private"
    else:
        visibility = "public"

    print(f"Attepting to sync {rhel_or_stream}/{project_name} MR !{merge_request_iid} downstream.")
    sync_mr_downstream(visibility, project_name, merge_request_iid)
