#!/usr/bin/python3

import os

from distrogitsync_utils import sync_branch_downstream


if __name__ == "__main__":

    namespace = os.environ.get("NAMESPACE")
    project_name = os.environ.get("CI_PROJECT_NAME")
    merge_request_target_branch = os.environ.get("CI_MERGE_REQUEST_TARGET_BRANCH_NAME")

    print(f"Attepting to sync {namespace}/{project_name} branch {merge_request_target_branch} downstream.")
    sync_branch_downstream(namespace, project_name, merge_request_target_branch)
