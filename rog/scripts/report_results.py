#!/usr/bin/python3

import argparse
import os
import sys

from gitlab_utils import fetch_pipeline_bridges, fetch_pipeline, fetch_pipeline_jobs, post_mr_comment, post_mr_note
from jinja2 import Environment, FileSystemLoader

COMMENT_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), "data")


def main():
    parser = argparse.ArgumentParser(
        description="Approve and comment on a GitLab MR based on pipeline results."
    )
    parser.add_argument("--project-id", required=True, help="GitLab Project ID")
    parser.add_argument("--pipeline-id", required=True, help="GitLab Pipeline ID")
    parser.add_argument("--merge-request-iid", required=True, help="Merge Request IID")
    parser.add_argument("--token", required=True, help="GitLab Private Token")

    args = parser.parse_args()

    headers = {"PRIVATE-TOKEN": args.token, "Content-Type": "application/json"}

    try:
        pipeline_bridges = fetch_pipeline_bridges(
            headers,
            args.project_id,
            args.pipeline_id,
        )
        test_stage_pipeline_id = pipeline_bridges[0]["downstream_pipeline"]["id"]

        pipeline = fetch_pipeline(
            headers, args.project_id, args.pipeline_id
        )
        pipeline_status = pipeline["detailed_status"]["text"]
        test_stage_pipeline = fetch_pipeline(
            headers, args.project_id, test_stage_pipeline_id
        )
        test_stage_pipeline_status = test_stage_pipeline["detailed_status"]["text"]

        pipeline_jobs = fetch_pipeline_jobs(
            headers, args.project_id, args.pipeline_id
        )
        check_tickets_status = next(
            (job["status"] for job in pipeline_jobs if job["name"] == "check_tickets"),
            None,
        )


        env = Environment(loader=FileSystemLoader(COMMENT_TEMPLATE_DIR), autoescape=True)

        if check_tickets_status != "success":
            template = env.get_template("check_tickets_failed.j2")
            comment_body = template.render()
            post_mr_comment(
                headers, args.project_id, args.merge_request_iid, comment_body
            )
        # Don't check pipeline_status here, as some jobs may pass with warnings and that's OK
        elif test_stage_pipeline_status == "Passed" and check_tickets_status == "success":
            template = env.get_template("pipeline_passed.j2")
            comment_body = template.render(pipeline_id=test_stage_pipeline_id)
            post_mr_note(
                headers, args.project_id, args.merge_request_iid, comment_body
            )
        # Check pipeline_status here, as the test stage pipeline may succeed but the CS build could fail
        elif test_stage_pipeline_status == "Warning" or pipeline_status == "Warning":
            jobs = fetch_pipeline_jobs(
                headers, args.project_id, test_stage_pipeline_id
            )
            successful_builds = [job for job in pipeline_jobs if job["status"] == "success" and "build" in job["name"]]
            successful_jobs = [job for job in jobs if job["status"] == "success"]
            all_successful = successful_builds + successful_jobs

            failed_builds = [job for job in pipeline_jobs if job["status"] != "success" and "build" in job["name"]]
            failed_jobs = [job for job in jobs if job["status"] != "success"]
            all_failed = failed_builds + failed_jobs

            template = env.get_template("pipeline_warning.j2")
            comment_body = template.render(
                pipeline_id=test_stage_pipeline_id,
                successful_jobs=all_successful,
                failed_jobs=all_failed,
            )
            post_mr_comment(
                headers, args.project_id, args.merge_request_iid, comment_body
            )
            sys.exit(1)
    except Exception as ex:
        print(f"Error: {ex}")
        sys.exit(1)


if __name__ == "__main__":
    main()
