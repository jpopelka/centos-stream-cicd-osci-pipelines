#!/usr/bin/python3

import requests


API_URL = "https://gitlab.com/api/v4"


def fetch_pipeline_bridges(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}/bridges",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()
    return response.json()


def fetch_pipeline(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()
    return response.json()


def fetch_pipeline_jobs(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}/jobs", headers=headers, timeout=10
    )
    response.raise_for_status()
    return response.json()


# Use discussions if you DO want to start a thread.
# Threads must be resolved; they prevent (auto)merge.
def post_mr_comment(headers, project_id, merge_request_iid, comment_body):
    data = {"body": comment_body}
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/discussions",
        headers=headers,
        json=data,
        timeout=10,
    )
    response.raise_for_status()


# Use notes if you DON'T want to start a thread.
# Threads must be resolved; they prevent (auto)merge.
def post_mr_note(headers, project_id, merge_request_iid, comment_body):
    data = {"body": comment_body}
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/notes",
        headers=headers,
        json=data,
        timeout=10,
    )
    response.raise_for_status()


def approve_mr(headers, project_id, merge_request_iid):
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/approve",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()
