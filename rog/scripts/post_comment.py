#!/usr/bin/python3

import argparse
import os
import sys
import gitlab


def main():
    parser = argparse.ArgumentParser(
        description="Post merge request comment."
    )
    parser.add_argument("--project-id", required=True, help="GitLab Project ID")
    parser.add_argument("--merge-request-iid", required=True, help="Merge Request IID")
    parser.add_argument("--comment-file", required=True, help="A file containing the comment")
    parser.add_argument("--as-discussion", required=False, action="store_true", default=False, help="Post the comment as discussion")

    args = parser.parse_args()

    if not os.getenv("GITLAB_TOKEN"):
        print("GITLAB_TOKEN is not set")
        sys.exit(1)

    with open(args.comment_file) as f:
        comment = f.read()

    if not comment:
        print(f"The comment file {args.comment_file} is empty")
        sys.exit(1)

    gl = gitlab.Gitlab(private_token=os.getenv("GITLAB_TOKEN"))
    try:
        project = gl.projects.get(args.project_id)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch project {args.project_id} - {e}")
        sys.exit(1)
    try:
        merge_request = project.mergerequests.get(args.merge_request_iid, lazy=True)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch merge request {args.merge_request_iid} - {e}")
        sys.exit(1)

    if args.as_discussion:
        merge_request.discussions.create({'body': comment})
    else:
        merge_request.notes.create({'body': comment})

if __name__ == "__main__":
    main()
