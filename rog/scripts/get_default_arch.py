#!/usr/bin/python3

import os
import sys

from specfile import Specfile

if __name__ == "__main__":

    rhel_or_stream = os.environ.get("RHEL_OR_STREAM")
    ci_project_name = os.environ.get("CI_PROJECT_NAME")

    specfile_path = f"/builds/redhat/{rhel_or_stream}/rpms/{ci_project_name}/{ci_project_name}.spec"
    try:
        specfile = Specfile(specfile_path)
    except FileNotFoundError:
        print(
            f"Warning: Could not open spec file {specfile_path}. Defaulting to x86_64.",
            file=sys.stderr,
        )
        print("x86_64")
        sys.exit(0)

    spec_arches = set()

    with specfile.tags() as tags:
        for tag in tags:
            if tag.name == "ExclusiveArch":
                spec_arches = spec_arches.union(set(tag.expanded_value.split()))
                break

    with specfile.tags() as tags:
        for tag in tags:
            if tag.name == "ExcludeArch":
                spec_arches = spec_arches.difference(set(tag.expanded_value.split()))

    if "x86_64" in spec_arches:
        result = "x86_64"
    elif any(arch in spec_arches for arch in ["ppc", "ppc64", "ppc64p7", "ppc64le"]):
        result = "ppc64le"
    elif any(arch in spec_arches for arch in ["s390", "s390x"]):
        result = "s390x"
    else:
        result = "x86_64"

    print(f"{result}")
