# CI Mediator configuration

This directory contains the configuration for the CI Mediator.

## Tests

Example of the `test` configuration:

```yaml
  osci.rpminspect:
    enabled: true
    context:
      - rhel8-rog
      - rhel9-rog
    input_artifacts:
      - brew-build
    depends_on: []
    metadata:
      owner: OSCI
      short_description: "rpminspect test"
      contact:
        email: osci-team@redhat.com
        slack: '#help-osci'
      docs: 'https://one.redhat.com/rhel-development-guide/#con_rpminspect_assembly_tools-and-services'
      issues:
        open: 'https://url.corp.redhat.com/rpminspect-open-bugs'
        new: 'https://issues.redhat.com/projects/OSCI/issues'
    trigger:
      strategy: "mediator"
```

* ___enabled___ (bool): Test is enabled or disabled
* ___context___ ([string]): Contexts in which is this test applicable
* ___input_artifacts___ ([string]): A list of artifacts that this test expects on input
* ___depends_on___ ([string]): A list of tests on which this test depends on
* ___metadata.owner___ (string): Owner of this test
* ___metadata.short_description___ (string): A short description of the test
* ___metadata.contact.email___ (string): Email
* ___metadata.contact.slack___ (string): Slack channel
* ___metadata.docs___ (string): Docs URL
* ___metadata.issues.open___ (string): Issue tracker URL for open issues for this test
* ___metadata.issues.new___ (string): Issue tracker URL for opening a new issue for this test
* ___trigger.strategy___ (string): The strategy for triggering the test. Options: mediator, webhook

## Components

Example of the `component` configuration:

```yaml
  rpms/osci-internal-test-package:
    tests:
      osci.rpminspect
```
