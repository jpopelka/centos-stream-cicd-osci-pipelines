To onboard new components to the RHEL-on-GitLab pipeline:

- Add new components to https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/blob/main/rog/configs/ci-mediator/config.yaml
    - [Example](https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/commit/a8c7429576f5c79d0fb06fa8664b888822e06900)
- Update workflow `rules` in https://gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/-/blob/latest/global-tasks.yml
    - [Example for enabling all branches](https://gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/-/commit/09db01d2c41adace6316d764e8bfe2fe799df23f)
    - To enable for `c10s` only, add the components to [this](https://gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/-/blob/latest/global-tasks.yml?ref_type=heads#L12) section.
- Disable Zuul CI for the components in https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/blob/master/resources/centos-distgits.yaml.
    - [Example disabling a single branch](https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/commit/47343f6a22aa4e285f4650b2b670c2e8dfa2dea3) (Note: this doesn't seem to be working -- a bug in Zuul(?))
    - [Example disabling Zuul completely](https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/commit/15ebbf1bd5b273a0ec0a4f8942e385c959fd1f79)
