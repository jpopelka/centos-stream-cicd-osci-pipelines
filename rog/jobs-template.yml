spec:
  inputs:
    included_pipeline_ref:
      default: 'main'
---

variables:
  ROG_IMAGE: "images.paas.redhat.com/osci/rog"
  ROG_IMAGE_TAG: "9f0faeef"
  BASE_IMAGE: "images.paas.redhat.com/osci/base-minimal"
  BASE_IMAGE_TAG: "9f0faeef"
  DISTROBUILDSYNC_IMAGE: "images.paas.redhat.com/osci/distrobuildsync"
  DISTROBUILDSYNC_IMAGE_TAG: "54b60d73"
  # TODO: The code for rog-konflux image above currently lives under
  #       https://gitlab.cee.redhat.com/vkabatov/prototypes
  #       and its content will need a permanent home when ready
  KONFLUX_IMAGE: "quay.io/rhel-devel-tools/rog-konflux"
  KONFLUX_IMAGE_TAG: "latest"

.with_artifacts:
  artifacts:
    when: always
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months

# Clone the pipeline repo to access scripts in the rog/scripts directory
.with_scripts:
  before_script:
    - |
      if [ $[[ inputs.included_pipeline_ref ]] = "main" ]; then
        echo "Production run, shallow-cloning pipeline repo"
        git clone --depth 1 https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
      else
        echo "Testing MR run, full-cloning pipeline repo"
        git clone https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
      fi
    - cd .rog-pipeline
    - git config advice.detachedHead false
    - |
      if [ $[[ inputs.included_pipeline_ref ]] != "main" ]; then
        echo "Fetching MR content..."
        git fetch origin "refs/$[[ inputs.included_pipeline_ref ]]:$[[ inputs.included_pipeline_ref ]]"
      fi
    - echo "Using ref $[[ inputs.included_pipeline_ref ]]."
    - git checkout $[[ inputs.included_pipeline_ref ]]
    - cd ..

# Template for handling retries for weird GitLab failures. This has nothing to do
# with retries for test execution issues.
.with_retries:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - unknown_failure
      - api_failure

.export_token: |
  if [[ "$RHEL_OR_STREAM" == "rhel" ]] && [[ "$NAMESPACE" == "rpms" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab-private.token"
  elif [[ "$RHEL_OR_STREAM" == "rhel" ]] && [[ "$NAMESPACE" == "tests" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab-private-tests.token"
  elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]] && [[ "$NAMESPACE" == "rpms" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab.token"
  elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]] && [[ "$NAMESPACE" == "tests" ]]; then
    GITLAB_TOKEN_FILENAME="gitlab-tests.token"
  fi
  export GITLAB_TOKEN=$(cat "/opt/secrets/$GITLAB_TOKEN_FILENAME")

.pick_build_options: |
  if [[ "$USE_DRAFTS" == "1" ]] ; then
    BUILD_TYPE="--draft"
  else
    # Current production use for RoG
    ARCH=$(./.rog-pipeline/rog/scripts/get_default_arch.py)
    ARCH_OVERRIDE="--arch-override=$ARCH"
    BUILD_TYPE="--scratch"
  fi

.check_if_rebase_needed:
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  script:
    - |
      if [[ "$CI_COMMIT_SHA" != "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]] ; then
        echo "The MR is out of sync with the target branch, please rebase and push again."
        exit 1
      fi

.pipeline_init:
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  extends: [.with_artifacts]
  script:
    - !reference [.export_token]
    - export GLAB_CONFIG_DIR=/tmp
    - |
      if [ ! "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        HAS_CONFLICTS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}" | jq ".has_conflicts")
        if [ "$HAS_CONFLICTS" = "false" ]; then
          echo "CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA"
          echo "This is a bug, proceed with caution and review the variables in the GitLab CI job logs."
          glab mr note -m "<h4>CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA<br/>This is a bug, proceed with caution.<br/>Saving environment in GitLab CI job logs.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        fi
        ROG_COMMIT_SHA=$CI_COMMIT_SHA
      else
        ROG_COMMIT_SHA=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      fi
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env
    - echo "Checking side-tag"
    - |
      MR_JSON=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}")
    - |
      SIDE_TAG=$(jq -r '.description' <<< "$MR_JSON" | grep '^side-tag:' || :)
    - |
      if [ -n "$SIDE_TAG" ]; then
        SIDE_TAG=$(echo "$SIDE_TAG" | sed 's/^.*: \?//')  # Remove "side-tag: " prefix
      fi
      echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - |
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        MR_COMMENT=""
        FORCE_LATEST_LABEL=""
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]] && [[ "$NAMESPACE" == "rpms" ]]; then
        MR_COMMENT="<ol><li>Do not forget to review this MR carefully if it comes from an external contributor.</li><li>The downstream target MR label is set to <code>target::latest</code> by default if you don't set another target when creating the MR. If you want to use a different target, choose another of the allowed targets and start a new pipeline:</li><ul><li><code>target::zstream</code>: z-stream</li><li><code>target::exception</code>: exception</li><li>You can use the <code>/label</code> command in a comment to set the label.</li></ul></ol>"
        FORCE_LATEST_LABEL="true"
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]] && [[ "$NAMESPACE" == "tests" ]]; then
        MR_COMMENT="<ol><li>Do not forget to review this MR carefully if it comes from an external contributor.</li></ol>"
        FORCE_LATEST_LABEL="true"
      fi
    - >
      if [ -n "$MR_COMMENT" ]; then
        glab mr note -m "$MR_COMMENT" --unique -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      fi
    - >
      if [ -n "$FORCE_LATEST_LABEL" ] && [ -z "$CI_MERGE_REQUEST_LABELS" ]; then
        curl -X PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" --data '{"labels": "target::latest"}' "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}" > /dev/null
      fi
    - export

.sync_mr_downstream:
  extends: [.with_artifacts, .with_retries, .with_scripts]
  needs:
    - job: pipeline_init
      optional: true
      artifacts: true
    - job: check_tickets
      optional: true
      artifacts: true
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  tags:
    - distrobaker
  script:
    - export
    - ./.rog-pipeline/rog/scripts/sync_mr_downstream.py
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env

.build_rpm:
  timeout: 24h
  artifacts:
    paths:
      - distrobuildsync.html
    expose_as: 'Build Brew RPM log'
  extends: [.with_artifacts, .with_retries, .with_scripts]
  needs:
    - job: sync_mr_downstream
      artifacts: true
  tags:
    - distrobaker
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  script:
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "ROG_COMMIT_SHA=$ROG_COMMIT_SHA" >> metadata.env
    - echo "SIDE_TAG=$SIDE_TAG"
    - kinit -k -t "/opt/secrets/osci-distrobuildsync.keytab" "osci-distrobuildsync@IPA.REDHAT.COM"
    - !reference [.export_token]
    - !reference [.pick_build_options]
    - |
      # Get the build target.
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        # Find leading digit(s) - RHEL major version
        X=$(grep -o '[0-9]\{1,2\}' <<< $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | head -1)
        PROFILE="c${X}s"
        # use pesign build target for kernel and kernel-rt
        REGEX='^kernel(-rt)?$'
        if [[ "$CI_PROJECT_NAME" =~ $REGEX ]]; then
          TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-z-candidate-pesign
        else
          TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-z-candidate
        fi
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        PROFILE=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      fi
    - |
      if [ -n "$SIDE_TAG" ]; then
        set +o pipefail
        DOWNSTREAM_SIDE_TAG=$(koji -p stream taginfo $SIDE_TAG | grep 'downstream_sidetag' | sed "s/.* '//;s/'//")
        if [ -z "$DOWNSTREAM_SIDE_TAG" ]; then
          echo "No downstream sidetag for $SIDE_TAG, exiting"
          exit 1 # TODO: Wait instead?
        fi
        ./.rog-pipeline/rog/scripts/wait_side_tag_sync.py $SIDE_TAG $DOWNSTREAM_SIDE_TAG
        TARGET=$DOWNSTREAM_SIDE_TAG
      fi
    - NS_COMPONENT="rpms/$CI_PROJECT_NAME"
    - IFS=',' read -r -a labels <<< "$CI_MERGE_REQUEST_LABELS"
    - |
      for label in "${labels[@]}"
      do
          if [[ $label == target::* ]]; then
              TARGET_LABEL="${label#target::}"
              break
          fi
      done
    - |
      if [ -z "$TARGET_LABEL" ]; then
        echo "Warning: MR label not set, using default 'target::latest'"
      fi
    - echo "Submitting brew build of ${NS_COMPONENT}#${ROG_COMMIT_SHA}, using distrobuildsync profile $PROFILE, target $TARGET, target label $TARGET_LABEL, build type $BUILD_TYPE"
    # TODO: Remove the -l debug once we know it works as expected
    # The -t and -p are used to specify build target.
    # If $TARGET is non-empty, it's used.
    # If $TARGET is not specified (empty) distrobuildsync uses target specified in the distrobaker config for the profile (-p $PROFILE).
    - ARCH=$(./.rog-pipeline/rog/scripts/get_default_arch.py)
    - python3 /tmp/distrobuildsync $BUILD_TYPE --arch-override $ARCH -l debug -b "${NS_COMPONENT}#${ROG_COMMIT_SHA}" -t "$TARGET" -p "$PROFILE" -i "${TARGET_LABEL:-latest}" https://gitlab.cee.redhat.com/osci/distrobaker_config.git#rhel9 2>&1 | tee distrobuildsync.log
    - echo "<html><body><h1>Distrobuildsync build log</h1><pre>" > distrobuildsync.html
    - cat distrobuildsync.log >> distrobuildsync.html
    - echo "</pre></body></html>" >> distrobuildsync.html
    # : INFO : Build submitted for rpms/cups, target rhel-9.5.0-candidate; task 60118525; SCMURL: git+https://pkgs.devel.redhat.com/git/rpms/cups#4aa5ef7...
    - TASK_ID=$(tail distrobuildsync.log | grep "Build submitted for " | grep -Eo ' task [0-9]+' | awk '{ print $2 }')
    - TARGET_SUBMITTED=$(tail distrobuildsync.log | grep 'Build submitted for ' | grep -Eo ' target [^;]+' | awk '{ print $2 }')
    - |
      if [ "$SIDE_TAG" ]; then
        NVR=$(koji -p stream --quiet list-builds --package="$CI_PROJECT_NAME" --reverse | head -n 1 | cut -d ' ' -f 1)
        PATTERN=$(echo $NVR | sed 's/\./\\./g')
        set +o pipefail
        ADDITIONAL_ARTIFACTS=$(koji -p stream list-tagged --quiet --latest $SIDE_TAG | cut -d ' ' -f 1 | grep -v "$PATTERN" | sed -z 's/\n/,/g')
      fi
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET_SUBMITTED" >> metadata.env
    - echo "ADDITIONAL_ARTIFACTS=$ADDITIONAL_ARTIFACTS" >> metadata.env
    - echo "Build submitted - https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=$TASK_ID"
    - brew watch-task "$TASK_ID"
    - echo "Everything went well!"

.build_centos_stream_rpm:
  timeout: 24h
  extends: [.with_artifacts, .with_retries, .with_scripts]
  needs:
    - job: pipeline_init
      optional: true
      artifacts: true
    - job: check_tickets
      optional: true
      artifacts: true
  tags:
    - distrobaker
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  script:
    - !reference [.pick_build_options]
    - kinit -k -t "/opt/secrets/osci-distrobuildsync.keytab" "osci-distrobuildsync@IPA.REDHAT.COM"
    - echo "Submitting koji $BUILD_TYPE-build!"
    - NS_COMPONENT="rpms/$CI_PROJECT_NAME"
    # scoped labels are mutually exclusive so no need for if-else
    - |
      if [ -n "$SIDE_TAG" ]; then
        TARGET=$SIDE_TAG
      else
        if [[ "$BUILD_TYPE" == "--draft" ]] ; then
          TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-draft
        else
          # use pesign build target for kernel and kernel-rt
          REGEX='^kernel(-rt)?$'
          if [[ "$CI_PROJECT_NAME" =~ $REGEX ]]; then
            TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-candidate-pesign
          else
            TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-candidate
          fi
        fi
      fi
    - echo "koji build target is $TARGET, build type is $BUILD_TYPE"
    - IFS=',' read -r -a labels <<< "$CI_MERGE_REQUEST_LABELS"
    - |
      for label in "${labels[@]}"
      do
          if [[ $label == target::* ]]; then
              RHEL_TARGET="${label#target::}"
              break
          fi
      done
    - |
      if [ -z "$RHEL_TARGET" ]; then
        echo "Warning: MR label not set, using default 'target::latest'"
      fi
    - |
      koji -p stream build $ARCH_OVERRIDE --fail-fast --nowait "$BUILD_TYPE" --custom-user-metadata "{\"rhel-target\": \"${RHEL_TARGET:-latest}\"}" "$TARGET" "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${ROG_COMMIT_SHA}" | tee koji_build.log
    - TASK_ID=$(cat koji_build.log | grep "^Created task:\ " | awk '{ print $3 }')
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET" >> metadata.env
    - koji -p stream watch-task "$TASK_ID"
    - echo "Everything went well!"


.discover_tests:
  extends: [.with_retries, .with_scripts]
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  artifacts:
    paths:
      - tier0_present.env # Generated file with TIER0_PRESENT=True/False value according to the check if tier0 are present
  script:
    - !reference [.export_token]
    # creates tier0_present.txt file with the result of the check
    - ./.rog-pipeline/rog/scripts/check_tier0_present.py



.generate_test_jobs:
  extends: [.with_artifacts, .with_retries, .with_scripts]
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  artifacts:
    paths:
      - test_runs.yml  # Generated dynamic pipeline
  needs:
    - job: build_rpm
      optional: true
      artifacts: true
    - job: discover_tests
      optional: true
      artifacts: true
  script:
    - export ROG_TIER0_PRESENT=$(cat "tier0_present.env")
    - echo "running python script that generates test jobs based on gating setup"
    - ./.rog-pipeline/./rog/scripts/generate_test_stage.py "rpms/$CI_PROJECT_NAME"  # Creates test_runs.yaml.
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "KOJI_TASK_ID=$KOJI_TASK_ID" >> metadata.env
    - echo "ADDITIONAL_ARTIFACTS=$ADDITIONAL_ARTIFACTS" >> metadata.env

.trigger_tests:
  allow_failure: true
  needs:
    - job: generate_test_jobs
      artifacts: true
  trigger:
    include:
      - artifact: test_runs.yml
        job: generate_test_jobs
    strategy: depend
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID  # So the test pipelines know where to get metadata from
    SIDE_TAG: $SIDE_TAG
    KOJI_TASK_ID: $KOJI_TASK_ID
    ADDITIONAL_ARTIFACTS: $ADDITIONAL_ARTIFACTS

.sync_branch_downstream:
  extends: [.with_retries, .with_scripts]
  tags:
    - distrobaker
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  script:
    - export
    - ./.rog-pipeline/rog/scripts/sync_branch_downstream.py

.promote_build:
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  tags:
    - distrobaker
  extends: [.with_retries, .with_scripts]
  needs:
    - job: sync_branch_downstream
  script:
    - !reference [.export_token]
    - echo "Checking the build source commit hash is synced to dist-git"
    # Inspect dist-git branch to see if the new HEAD commit is the source commit for the build.
    - DOWNSTREAM_SHA=$(git ls-remote "https://pkgs.devel.redhat.com/git/${NS_COMPONENT}/" "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" | cut -f1)
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
      fi
    - |
      if [[ "$DOWNSTREAM_SHA" != "$REF" ]]; then
        echo "The source commit for this draft build has not been synced downstream - not promoting this build."
        exit 1
      fi
    - kinit -k -t "/opt/secrets/osci-distrobuildsync.keytab" "osci-distrobuildsync@IPA.REDHAT.COM"
    - echo "Using Brew/Koji API to promote draft build(s) to production"
    - ./.rog-pipeline/rog/scripts/promote_build.py

.report_results:
  extends: [.with_retries, .with_scripts]
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  needs: [trigger_tests]
  script:
    - !reference [.export_token]
    - ./.rog-pipeline/rog/scripts/report_results.py --token $GITLAB_TOKEN --project-id $CI_PROJECT_ID --pipeline-id $CI_PIPELINE_ID --merge-request-iid $CI_MERGE_REQUEST_IID

.approve_mr:
  extends: [.with_retries, .with_scripts]
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  needs:
    - job: report_results
  # OSCI-7933 - For now, approve the MR if the builds succeed. Stabilise the pipeline before requiring tests to succeed.
  script:
    - !reference [.export_token]
    - ./.rog-pipeline/rog/scripts/approve_mr.py --token $GITLAB_TOKEN --project-id $CI_PROJECT_ID --pipeline-id $CI_PIPELINE_ID --merge-request-iid $CI_MERGE_REQUEST_IID

.discover_builds:
  extends: [.with_artifacts, .with_retries]
  tags:
    - distrobaker
  image: $DISTROBUILDSYNC_IMAGE:$DISTROBUILDSYNC_IMAGE_TAG
  needs:
    - job: pipeline_init
  script:
    - echo "Finding latest build for $TEST_PROFILE"
    - |
      BUILD=$(brew latest-build --quiet $TEST_PROFILE $CI_PROJECT_NAME | cut -d ' ' -f1)
      # If no build is found, try look for a -pending build
      if [[ -z "$BUILD" ]]; then
        BUILD=$(brew latest-build --quiet $TEST_PROFILE-pending $CI_PROJECT_NAME | cut -d ' ' -f1)
      fi
      # If still no build is found, try look for a -candidate build
      if [[ -z "$BUILD" ]]; then
        BUILD=$(brew latest-build --quiet $TEST_PROFILE-candidate $CI_PROJECT_NAME | cut -d ' ' -f1)
      fi
      # Stop looking for builds, we don't want to pick an un-gated build.
    - TEST_PROFILE_PREFIX="${TEST_PROFILE%%.*}"
    - echo "${TEST_PROFILE_PREFIX//-/_}=$BUILD" >> metadata.env
    - echo "Discovered $BUILD for $TEST_PROFILE"
  parallel:
    matrix:
      - TEST_PROFILE: [rhel-8.10.0, rhel-9.6.0, rhel-10.0]

.tier0:
  extends: [.with_retries]
  tags:
    - distrobaker
  image: $ROG_IMAGE:$ROG_IMAGE_TAG
  needs:
    - job: discover_builds
  allow_failure:
    exit_codes:
      - 77
  script:
    - |
      if [[ -z "${!BUILD}" ]]; then
        echo "No build was discovered for $BUILD. Skipping tier0 test for this release."
        exit 77
      else
        echo "Discovered ${!BUILD} for $BUILD; Triggering tier0 test for this release."
      fi
  parallel:
    matrix:
      - BUILD: [rhel_8, rhel_9, rhel_10]
